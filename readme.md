# Love packager

This is a small script to package distributable binaries of a love2d game.
cd into the folder containing your game assets, and then run the script from there. 
It will dump the results in a folder called out.


Things to note:
- Currently generates windows and linux builds. Linux builds use the portable AppImage version of love, so they should work on whatever distro.
- Default love version is currently 11.3, edit the number at the start of the script to change it.
- The script is designed to run on linux. It might work on windows in git bash, I haven't tried.
