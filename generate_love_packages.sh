#!/bin/bash

set -e
set -x

VERSION=11.4


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

GAME_DIR=$PWD

linux_love_binary=https://github.com/love2d/love/releases/download/$VERSION/love-$VERSION-x86_64.AppImage
windows_love_binary=https://github.com/love2d/love/releases/download/$VERSION/love-$VERSION-win64.zip
osx_love_binary=https://github.com/love2d/love/releases/download/$VERSION/love-$VERSION-macos.zip

build_windows() {
    echo "generating windows build"

    win_binary_dir="$DIR/win_$VERSION"

    if [ ! -e "$win_binary_dir" ]; then
        wget "$windows_love_binary" -O "$DIR/win_$VERSION.zip"

        pushd "$DIR" > /dev/null
        mkdir win_$VERSION
        cd win_$VERSION
        unzip "$DIR/win_$VERSION.zip" || (rm -rf "$win_binary_dir"; exit 1)
        popd > /dev/null
    fi

    mkdir -p "$OUTPUT/win"
    find "$DIR/win_$VERSION/love-"* -name "*.dll" -exec cp {} "$OUTPUT/win" \;
    find "$DIR/win_$VERSION/love-"* -name "love.exe" -exec cp {} "$OUTPUT/win" \;

    cat "$OUTPUT/win/love.exe" "$OUTPUT/tmp.zip" > "$OUTPUT/tmpexe"
    rm "$OUTPUT/win/love.exe"
    mv "$OUTPUT/tmpexe" "$OUTPUT/win/$GAME_NAME.exe"
    chmod +x "$OUTPUT/win/$GAME_NAME.exe"
}

build_linux() {
    echo "generating linux build"

    if [ ! -e "$DIR/love_linux_$VERSION" ]; then
        wget "$linux_love_binary" -O "$DIR/love_linux_$VERSION" || (rm $linux_love_binary; exit 1)
    fi

    mkdir -p "$OUTPUT/linux"
    cp "$DIR/love_linux_$VERSION" "$OUTPUT/linux/love"
    chmod +x "$OUTPUT/linux/love"

    cp "$OUTPUT/tmp.zip" "$OUTPUT/linux/assets.zip"

    echo '#!/bin/bash' > "$OUTPUT/linux/$GAME_NAME.sh"
    echo 'DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"' >> "$OUTPUT/linux/$GAME_NAME.sh"
    echo 'cd $DIR' >> "$OUTPUT/linux/$GAME_NAME.sh"
    echo './love ./assets.zip' >> "$OUTPUT/linux/$GAME_NAME.sh"
    chmod +x "$OUTPUT/linux/$GAME_NAME.sh"
}

build_osx() {
    echo "generating osx build"

    osx_binary_dir="$DIR/osx_$VERSION/love.app"

    if [ ! -e "$osx_binary_dir" ]; then
        wget "$osx_love_binary" -O "$DIR/osx_$VERSION.zip"

        pushd "$DIR" > /dev/null
        mkdir osx_$VERSION
        cd osx_$VERSION
        unzip "$DIR/osx_$VERSION.zip" || (rm -rf "$osx_binary_dir"; exit 1)
        popd > /dev/null
    fi

    mkdir -p "$OUTPUT/osx"
    cp -r "$osx_binary_dir" "$OUTPUT/osx/$GAME_NAME.app"

    cp "$OUTPUT/tmp.zip" "$OUTPUT/osx/$GAME_NAME.app/Contents/Resources/$GAME_NAME.love"

    sed -ibak "s/<string>LÖVE<\/string>/<string>$GAME_NAME<\/string>/" "$OUTPUT/osx/$GAME_NAME.app/Contents/Info.plist"
    sed -ibak "s/<string>org.love2d.love<\/string>/<string>org.$GAME_ID.$GAME_ID<\/string>/" "$OUTPUT/osx/$GAME_NAME.app/Contents/Info.plist"

    # remove the file associations section
    sed -ibak '/<key>UTExportedTypeDeclarations<\/key>/,$d' "$OUTPUT/osx/$GAME_NAME.app/Contents/Info.plist"
    echo "</dict>" >> "$OUTPUT/osx/$GAME_NAME.app/Contents/Info.plist"
    echo "</plist>" >> "$OUTPUT/osx/$GAME_NAME.app/Contents/Info.plist"
}

if [ "$#" -ne 1 ]; then
    echo "Error, Usage: $0 \"My Cool Game\""
    exit 1
fi

GAME_NAME=$1
GAME_ID=`echo $GAME_NAME | sed 's/ /_/g'`
OUTPUT_BASE="$PWD/love_pkg_output"
OUTPUT="$OUTPUT_BASE/$GAME_ID"

echo $GAME_ID

rm -rf "$OUTPUT_BASE" || true
mkdir -p "$OUTPUT"
zip -r "$OUTPUT/tmp.zip" . --exclude ".git/*" --exclude out >/dev/null

build_windows
build_linux
build_osx

rm "$OUTPUT/tmp.zip"

echo "generating zip file"
pushd  "$OUTPUT_BASE" >/dev/null
zip -ry "$GAME_ID.zip" "$GAME_ID" >/dev/null
popd > /dev/null

echo "done, generated in $OUTPUT_BASE"
